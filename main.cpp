#include <iostream>
#include <utility>

using std::string;

class Animal {
private:
    int height;
    int width;
    int weight;
    string voice;
    int movement_speed;
public:
    Animal(int _height, int _width, int _weight, string _voice, int _movement_speed) {
        height = _height;
        width = _width;
        weight = _weight;
        voice = std::move(_voice);
        movement_speed = _movement_speed;
    }

    void printSizes() const {
        printf("H: %d; W: %d;", height, width);
    }

    void move() const {
        printf("Moved for %d km", movement_speed);
    }

    void say() const {
        printf(voice.c_str());
    }
};
class Bird: public Animal {
private:
    int flying_speed;
    int flying_limit;
public:
    Bird(int _height, int _width, int _weight, int _movement_speed, int _flying_speed, int _flying_limit)
    : Animal(_height, _weight, _weight, "twi twi", _movement_speed) {
        flying_speed = _flying_speed;
        flying_limit = _flying_limit;
    };

    void fly() const {
        printf("Bird flown %d km", flying_speed);
    }

};
class Human: public Animal {
private:
    string fullname;
    string work;
    int money;
public:
    Human(int _height, int _width, int _weight, int _movement_speed, string _fullname, string _work, int _money)
            : Animal(_height, _weight, _weight, "blah blah", _movement_speed) {
        fullname = std::move(_fullname);
        work = std::move(_work);
        money = _money;
    };

    void printStats() {
        printf("[%s] work at '%s' and have %d$", fullname.c_str(), work.c_str(), money);
    }

    void earnMoney(int amount) {
        money += amount;
    }
};

int main() {
    return 0;
}
